<?php 
    require_once 'login.php';
    require_once 'print_sites.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Memos</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Ranchers&display=swap" rel="stylesheet">   
    <link href="styles.css" rel="stylesheet">
</head>
<body>
   
<?php 

    if(isset($_POST['pswd-ajout'])){  //champ présent dans login.php
        if($_POST['pswd-ajout'] == $clef) {
            include_once 'ajoutSite.php';
           
        } else {
            echo "Erreur d'authentification";
        }
    }
    if(isset($_POST['pswd-supp'])){  //champ présent dans login.php
        if($_POST['pswd-supp'] == $mdproot) {
            include_once 'suppSite.php';
           
        } else {
            echo "Erreur d'authentification";
        }
    }

?>



<h1>Memos</h1>
<h2>Optimisation</h2>
<?php
//affichage des sites depuis la bdd
    //require_once('print_sites.php');
    print_site_by_theme("optimisation");

?>
<h2>Couleurs</h2>
<?php print_site_by_theme("couleurs"); ?>

<h2>Compatibilité</h2>
<?php print_site_by_theme("compatibilité"); ?>

<h2>Tutoriels</h2>
<?php print_site_by_theme("tutoriel"); ?>

<h2>API</h2>
<?php print_site_by_theme("API"); ?>
    
</body>
</html>