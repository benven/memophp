<?php

//connect to DATABASE
  $DB_DSN = 'mysql:host=localhost;dbname=form_sites';
  $DB_USER = 'root';
  $DB_PASS = '4620';
  $options = 
  [
      PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
      PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
  ];

try {
    $pdo = new PDO($DB_DSN, $DB_USER, $DB_PASS, $options);

    //DELETE 
    $sql = "DELETE FROM sites WHERE nom_site = :nom_site";
    $stmt = $pdo->prepare($sql);
    $stmt->bindValue(':nom_site', $_POST['delete-site']);
    $stmt->execute();
    echo "Supprimé";
    header('Location: index.php');
    
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
    
}

?>