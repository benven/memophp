<?php

//connect to DATABASE
  $DB_DSN = 'mysql:host=localhost;dbname=form_sites';
  $DB_USER = 'root';
  $DB_PASS = '4620';
  $options = 
  [
      PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
      PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
  ];

try {
    $pdo = new PDO($DB_DSN, $DB_USER, $DB_PASS, $options);

    //INSERT DATA
    $sql = "INSERT INTO sites (nom_site, url_site, commentaire_site, theme) VALUES (:nom_site, :url_site, :commentaire_site, :theme)";
    $stmt = $pdo->prepare($sql);

    $nom_site = $_POST['site'];
    $url_site = $_POST['url'];
    $commentaire_site = $_POST['commentaire'];
    $theme = $_POST['theme'];

    $stmt->bindValue(':nom_site', $nom_site);
    $stmt->bindValue(':url_site', $url_site);
    $stmt->bindValue(':commentaire_site', $commentaire_site);
    $stmt->bindValue(':theme', $theme);
    if(!empty($nom_site) && !empty($url_site) && !empty($commentaire_site)){
        $stmt->execute();
        echo "site ajouté";
        header('Location: index.php');
    } else {
        echo "erreur";
    }
   

    
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
//UPDATE DATA
/*
$sql = "UPDATE pets SET microchip = :micro WHERE owner = :owner AND petname = :petname";
$stmt = $pdo->prepare($sql);
$stmt->bindValue(':micro', '121342345');
$stmt->bindValue(':owner', 'Jamie');
$stmt->bindValue(':petname', 'Max');

$stmt->execute();

//DELETE DATA
$sql = "DELETE FROM pets WHERE owner = :owner AND petname = :petname";
$stmt = $pdo->prepare($sql);
$stmt->bindValue(':owner', 'Ted');
$stmt->bindValue(':petname', 'Angel');
$stmt->execute();
*/